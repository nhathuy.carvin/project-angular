import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatButtonModule,
  MatProgressSpinnerModule,
  MatSidenavModule } from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { MenuSearchComponent } from './layout/nav/bootstrap/menu-search/menu-search.component';
import { MenuDropdrownComponent } from './layout/nav/bootstrap/menu-dropdown/menu-dropdown.component';
import { MenuLogoComponent } from './layout/nav/bootstrap/menu-logo/menu-logo.component';
import { MenuComponent } from './layout/nav/material/menu/menu.component';
import { MenuLogoContentComponent } from './layout/content/bootstrap/menu-logo-content/menu-logo-content.component';
import { MenuDropdownContentComponent } from './layout/content/bootstrap/menu-dropdown-content/menu-dropdown-content.component';
import { MenuSearchContentComponent } from './layout/content/bootstrap/menu-search-content/menu-search-content.component';
import { MenuContentComponent } from './layout/content/material/menu-content/menu-content.component';
import { LoginComponent } from './modules/login/page/login.component';

@NgModule({
  declarations: [
    AppComponent,

    MenuSearchComponent,
    MenuDropdrownComponent,
    MenuComponent,
    MenuLogoComponent,
    MenuLogoContentComponent,
    MenuDropdownContentComponent,
    MenuSearchContentComponent,
    MenuContentComponent,
    LoginComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatSidenavModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
