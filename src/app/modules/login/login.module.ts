import { NgModule } from '@angular/core';

import { LoginComponent } from './page/login.component';
import { HomeRoutingModule } from './login.routing';

@NgModule({
    declarations: [
        LoginComponent,
    ],
    imports: [
        HomeRoutingModule
    ],
    exports: [],
    providers: [],
    entryComponents: []
})
export class HomeModule {}