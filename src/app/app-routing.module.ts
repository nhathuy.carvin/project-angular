import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuLogoContentComponent } from './layout/content/bootstrap/menu-logo-content/menu-logo-content.component';
import { MenuDropdownContentComponent } from './layout/content/bootstrap/menu-dropdown-content/menu-dropdown-content.component';
import { MenuSearchContentComponent } from './layout/content/bootstrap/menu-search-content/menu-search-content.component';
import { MenuContentComponent } from './layout/content/material/menu-content/menu-content.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: () =>
    import('./modules/home/home.module').then(md => md.HomeModule)
  },
  {
    path: 'login',
    loadChildren: () =>
    import('./modules/login/login.module').then(md => md.HomeModule)
  },
  {
    path: 'logo',
    component: MenuLogoContentComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
        import('./modules/home/home.module').then(md => md.HomeModule)
      }
    ]
  },
  {
    path: 'search',
    component: MenuSearchContentComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
        import('./modules/home/home.module').then(md => md.HomeModule)
      }
    ]
  },
  {
    path: 'dropdown',
    component: MenuDropdownContentComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
        import('./modules/home/home.module').then(md => md.HomeModule)
      }
    ]
  },
  {
    path: 'material',
    component: MenuContentComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
        import('./modules/home/home.module').then(md => md.HomeModule)
      }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
